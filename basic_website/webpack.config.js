const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer')

const extractStyles = new ExtractTextPlugin({
    filename: "[name].css",
    disable: false
});

const stylesSource = [
    path.resolve(__dirname, 'stylesheets/src/all/all.scss')
]

const getAllStylesSources = () => {
    return stylesSource
}

const scssConfig = {
    entry: {
        main: getAllStylesSources()
    },
    output: {
        path: __dirname + '/public/styles/main/',
        filename: '[name].css'
    },
    module : {
        rules: [{
            test: /\.scss$/,
            use: extractStyles.extract({
                use: [{
                    loader: "css-loader",
                    options: {
                        modules: true,
                        importLoaders: true,
                        localIdentName: "[local]"
                    }
                },{
                    loader: 'postcss-loader',
                    options: {
                        ident: 'postcss',
                        plugins: () => [ autoprefixer() ]
                    }
                },{
                    loader: "sass-loader"
                }],
                // use style-loader in development
                fallback: "style-loader"
            })
        }]
    },
    plugins: [
        extractStyles
    ]
};


const JS_SRC = path.resolve(__dirname, 'javascripts/src/all');
const JS_DIST = path.resolve(__dirname, 'public/scripts/main');

const jsConfig = {
    entry: JS_SRC + '/app.js',
    output: {
        path: JS_DIST,
        filename: 'main.js'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json']
    },
    module : {
        rules : [
            {
                test : /\.jsx?/,
                include : JS_SRC,
                loader : 'babel-loader'
            }
        ]
    }
};

module.exports = [
    scssConfig,
    jsConfig
]
