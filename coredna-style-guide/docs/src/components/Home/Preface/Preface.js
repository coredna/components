import React from 'react';
import { PageBlock, Section, Columns, Text } from 'seek-style-guide/react';

export default () => (
  <PageBlock>
    <Section header>
      <Text hero>A principled design process</Text>
    </Section>

    <Section>
      <Text subheading regular>Coredna Style Guide is inspired by SEEK Style Guide. The purpose is to enable the creation of content that will assist our partners to complete tasks easily and hopefully enjoy the experience.</Text>
    </Section>
  </PageBlock>
);
