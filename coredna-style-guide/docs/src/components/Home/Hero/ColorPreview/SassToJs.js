const sassToJs = function (sheet) {
    let lessVars = {};
    let matches = sheet.match(/\$(.*:[^;]*)/g) || [];
    matches.forEach(function (variable) {
        let definition = variable.split(/:\s*/);
        lessVars[definition[0].replace(/['"]+/g, '').trim()] = definition.splice(1).join(':');
    });
    return lessVars;
};

export default sassToJs
