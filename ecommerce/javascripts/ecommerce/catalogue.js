import $ from 'jquery'
import {CURRENCY, CURRENCY_CODE} from './global'

const Catalogue = (() => {

    const product = '.ecommerce-list-item-data'

    const currentItem = '[data-ecomm-product]'

    const productLink = '.ecommerce-product-link'

    const relatedProduct = '.ecommerce-related-item-data'

    const getListData = ($form, price) => {
        if($form.length === 1) {
            let formData = $form.serializeArray()
            if(price){
                formData.push({name: 'price', value: CURRENCY + price})
            }
            return formData.map(item=>item.value).join(', ')
        }
        else {
            return ''
        }
    }

    const getProduct = (productId) => {
        let productDataId = '[data-id=' + productId + ']'
        return $(product + productDataId)
    }

    const updateListItemData = (variant, productId) => {
        let $product = getProduct(productId)
        $product.data('price', variant.price.value)
        $product.data('variant', variant.name)
        // send new variant detail
        sendProductDetailImpression('prodDetailVariant', productId)
    }

    const getImpressionsData = ($data) => {
        return $.makeArray($data.find(product)).map(item=>Object.assign({}, $(item).data()))
    }

    const getProdImpressionsData = ($relatedProducts) => {
        return $relatedProducts.length > 0 ? $.makeArray($relatedProducts).map(item=>Object.assign({}, $(item).data())) : []
    }

    const sendAddRemoveAction = (action, eventType, productId, quantity) => {
        let $product = getProduct(productId)
        let data = Object.assign({}, $product.data())
        data.quantity = quantity
        delete data.list
        delete data.position
        dataLayer.push({
            'event': eventType,
            'ecommerce': {
                'currencyCode': CURRENCY_CODE,
                'impressions': getProdImpressionsData($(relatedProduct)),
                [action]: {
                    'products': new Array(data)
                }
            }
        });
    }

    const sendImpressions = ($data) => {
        dataLayer.push({
            'ecommerce': {
                'currencyCode': 'USD',
                'impressions': getImpressionsData($data)
            },
            'event': 'prodListing'
        });
    }

    const sendProductDetailImpression = (eventType, productId) => {
        let data = getProdImpressionsData(getProduct(productId))
        let listName = data.length > 0 ? data[0].category : 'Shop'
        dataLayer.push({
            'ecommerce': {
                'detail': {
                    'actionField': {'list': listName},
                    'products': data
                }
            },
            'event': eventType
        });
    }

    const sendProductClick = (data, url) => {
        let products = new Array(data)
        dataLayer.push({
            'event': 'productClick',
            'ecommerce': {
                'click': {
                    'products': products
                }
            },
            'eventCallback': function(){
                window.location.href = url;
            }
        })
    }

    const getPublicFunctions = () => {
        return {
            getListData: getListData,
            sendImpressions: sendImpressions,
            updateListItemData: updateListItemData,
            sendAddRemoveAction: sendAddRemoveAction,
            sendProductDetailImpression: sendProductDetailImpression
        }
    }

    const events = () => {
        $('body').on('click', productLink, (event) => {
            event.preventDefault()
            let url = event.currentTarget.href
            let item = $(event.currentTarget).closest(currentItem)
            let data = Object.assign({}, item.find(product).data())
            delete data.list
            sendProductClick(data, url)
        })
    }

    const init = () => {
        events()
    }

    return {
        init: init,
        getFunctions: getPublicFunctions
    }

})()

export default Catalogue