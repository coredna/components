import Catalogue from './catalogue'

console.log('%c COREDNA E-COMMERCE ', 'background: #ee1d3e; color: #ffffff');

Catalogue.init()

window.COREDNA_ECOMM = {
    catalogue: Catalogue.getFunctions()
}
